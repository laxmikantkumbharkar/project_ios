//
//  SearchCollectionViewCell.swift
//  Dairy_app
//
//  Created by sunbeam on 25/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
}
