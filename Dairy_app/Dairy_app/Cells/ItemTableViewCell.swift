//
//  ItemTableViewCell.swift
//  Dairy_app
//
//  Created by sunbeam on 22/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    
    
    
    @IBOutlet weak var imageview: UIImageView!
    
    
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var buttonAdd: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var labelQty: UILabel!
    
    var onAddItem: (() -> Void)!
    var onQtyChange: ((Int) -> Void)!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onStepper() {
        labelQty.text = "\(Int(stepper.value))"
        onQtyChange(Int(stepper.value))
    }
    @IBAction func onAdd() {
        onAddItem()
    }
    

}
