//
//  ProfileViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 22/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(logout))
        // Do any additional setup after loading the view.
    }
    
    @objc func logout(){
        AppPreferences.logout()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showLogin()
    }

    override func viewWillAppear(_ animated: Bool) {
        loadProfile()
    }
    func loadProfile(){
        makeApiCall(api: "/customer/\(AppPreferences.customerId)",
            onSuccess: { result in
                let customer = result as! [String: Any]
                print(customer)
                self.labelName.text = customer["Cname"] as? String
                self.labelAddress.text = customer["Carea"] as? String
                self.labelPhone.text = customer["Phone"] as? String
                
                let imageUrl = URL(string: self.url + "/\( customer["Cthumbnail"] as! String)")
                print(imageUrl)
                self.imageView.kf.setImage(with: imageUrl)
        }, method : .get)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
