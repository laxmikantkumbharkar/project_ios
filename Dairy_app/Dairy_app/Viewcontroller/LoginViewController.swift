//
//  LoginViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 19/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit
import Toast_Swift

class LoginViewController: BaseViewController {

    @IBOutlet weak var editUsername: UITextField!
    @IBOutlet weak var editPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onLogin() {
        if editUsername.text!.count == 0 {
            showWarning(message: "Please enter username")
        }else if editPassword.text!.count == 0{
            showWarning(message: "Please enter password")
        }else{
            let body = [
                "Cusername": editUsername.text!,
                "Cpassword": editPassword.text!
            ]
            
            makeApiCall(api: "/customer/signin", onSuccess: {(result) in
                self.view.makeToast("Welcome to application")
                
                let user = result as! [String: Any]
                let Cname = user["Cname"] as! String
                let Id = user["Id"] as! Int
                let Cemail = user["Cemail"] as! String
                
                
                AppPreferences.loginCustomer(Cname: Cname, Cemail: Cemail, Id: Id)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showHome()
            }, onError: { error in
                self.view.makeToast("invalid username or password")
            }, method: .post,parameters: body)
        }
        
    }
    

    @IBAction func onRegister() {
    }
}
