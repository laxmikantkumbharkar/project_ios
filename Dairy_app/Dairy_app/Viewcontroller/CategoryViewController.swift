//
//  CategoryViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 26/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit

class CategoryViewController: BaseViewController {

    @IBOutlet weak var categorycollectionView: UICollectionView!
    var products: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        loadProducts()
//    }
//    func loadProducts(){
//        makeApiCall(api: "/product", onSuccess: { result in
//            let array = result as! [[String: Any]]
//            for item in array{
//                let product = Product()
//                product.Pname = item["Pname"] as? String
//                product.Prate = item["Prate"] as? Double
//                product.PQuantity = item["PQuantity"] as? String
//                product.Pthumbnail = item["Pthumbnail"] as? String
//                print(product.Pthumbnail)
//                self.products.append(product)
//            }
//            self.categorycollectionView.reloadData()
//        },method: .get)
//    }
    
    
    @IBAction func onMilk() {
        makeApiCall(api: "/milk", onSuccess: { result in
            self.products.removeAll()
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
//                product.Prate = item["Prate"] as? Double
//                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.categorycollectionView.reloadData()
        },method: .get)
    }
    
    @IBAction func onButter() {
        makeApiCall(api: "/butter", onSuccess: { result in
            self.products.removeAll()
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
                //                product.Prate = item["Prate"] as? Double
                //                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.categorycollectionView.reloadData()
        },method: .get)
    }
    
    
    @IBAction func onPaneer() {
        makeApiCall(api: "/paneer", onSuccess: { result in
            self.products.removeAll()
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
                //                product.Prate = item["Prate"] as? Double
                //                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.categorycollectionView.reloadData()
        },method: .get)
    }
    
    @IBAction func onIcecream() {
        makeApiCall(api: "/icecream", onSuccess: { result in
            self.products.removeAll()
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
                //                product.Prate = item["Prate"] as? Double
                //                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.categorycollectionView.reloadData()
        },method: .get)
    }
    
    @IBAction func onGhee() {
        makeApiCall(api: "/ghee", onSuccess: { result in
            self.products.removeAll()
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
                //                product.Prate = item["Prate"] as? Double
                //                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.categorycollectionView.reloadData()
        },method: .get)
    }
    
}

extension CategoryViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
        
        let product = products[indexPath.row]
        
        cell.labelTitle.text = product.Pname
        //        cell.labelInfo.text = "\(product.Prate!) | \(product.PQuantity!)"
        print(product.Prate)
        print(product.PQuantity)
        let imageUrl = URL(string: url + "/\(product.Pthumbnail!)")
        print(imageUrl)
        cell.categorycollectionView.kf.setImage(with: imageUrl)
        cell.categorycollectionView.layer.cornerRadius = 10
        return cell
    }
}

extension CategoryViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        //        let product = products[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ItemListViewController") as! ItemListViewController
        //        vc.product = product
        present(vc,animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
}

extension CategoryViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width / 2) - 10
        return CGSize(width: width, height: 200)
    }
}

