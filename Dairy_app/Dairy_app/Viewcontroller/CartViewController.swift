//
//  CartViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 23/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var items: [Product] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadItem()
    }
    
    func loadItem(){
        makeApiCall(api: "/cart/\(AppPreferences.customerId)",
                    onSuccess: { response in
                        self.items.removeAll()
                        let array = response as! [[String: Any]]
                        for obj in array {
                            let item = Product()
                            item.Pid = obj["Oid"] as? Int
                            item.Pname = obj["Pname"] as? String
                            item.Prate = obj["Prate"] as? Double
                            item.PQuantity = obj["PQuantity"] as? String
                            item.Pthumbnail = obj["Pthumbnail"] as? String
                            item.Punit = obj["Punit"] as? Int
                            self.items.append(item)
                        }
                        
                        self.tableView.reloadData()
        }, method: .get)
    }
    
    
    @IBAction func onBuynow() {
        view.makeToast("Thank you..!!")
//        let body = [
//            "Cid": "\(AppPreferences.customerId)",
//            //"total": editAddress.text!
//        ]
//        makeApiCall(api: "/orders", onSuccess: {(result) in
//            self.view.makeToast("Successfully ordered")
//            self.navigationController?.popViewController(animated: true)
//        },parameters: body)
        
    }
    
}


extension CartViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        let item = items[indexPath.row]
        
        cell.labelName.text = item.Pname
        cell.labelPrice.text = "\(item.Prate!)"
        cell.labelQuantity.text = item.PQuantity
        
        let imageUrl = URL(string: url + "/\(item.Pthumbnail!)")
        
        cell.imageview.kf.setImage(with: imageUrl)
        cell.imageview.layer.cornerRadius = 10
    
        cell.labelQty.text = "\(item.Punit!)"
        cell.stepper.value = Double(item.Punit!)
        
        cell.onQtyChange = { qty in
            
            if qty == 0{
                self.makeApiCall(api: "/cart/\(item.Pid!)",
                    onSuccess: { response in
                        self.loadItem()
                }, method: .delete)
            }else{
            
                let body = [
                "Quantity": qty
                ]
            
            self.makeApiCall(api: "/cart/\(item.Pid!)",
                onSuccess: { response in
                
            }, method: .put, parameters: body)
            }
        }
        
//        print(item.Punit)
        
        return cell
    }
}

extension CartViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
