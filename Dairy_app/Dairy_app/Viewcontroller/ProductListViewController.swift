//
//  ViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 18/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit
import Kingfisher

class ProductListViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var products: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Products"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadProducts()
    }
    func loadProducts(){
        makeApiCall(api: "/product", onSuccess: { result in
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
                product.Prate = item["Prate"] as? Double
                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.collectionView.reloadData()
        },method: .get)
    }
}

extension ProductListViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! ProductCollectionViewCell
        
        let product = products[indexPath.row]
    
        cell.labelName.text = product.Pname
        cell.labelInfo.text = "\(product.Prate!) | \(product.PQuantity!)"

        print(product.Prate)
        print(product.PQuantity)
        let imageUrl = URL(string: url + "/\(product.Pthumbnail!)")
        print(imageUrl)
        cell.imageViewProduct.kf.setImage(with: imageUrl)
        cell.imageViewProduct.layer.cornerRadius = 10
        return cell
    }
}

extension ProductListViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
//        let product = products[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ItemListViewController") as! ItemListViewController
//        vc.product = product
        present(vc,animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
}

extension ProductListViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (view.frame.width / 2) - 10
        return CGSize(width: width, height: 200)
    }
}

