//
//  ItemListViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 22/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit
import Kingfisher
import Toast_Swift

class ItemListViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
       var items: [Product] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Back() {
         dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadItem()
    }
    
//    @IBAction func showSecondTab(sender: AnyObject) {
//        let  navController = self.tabBarController?.viewControllers![2] as! UINavigationController
//        ///secondviewcontroller in your case is cart
//        let cartViewController = navController.viewControllers[0] as! CartViewController
//        //set values you want to pass
//        //lets say I want to pass name to secondVC
//        //secondViewController.name = "ABCD"
//
//        self.tabBarController?.selectedIndex = 2
//    }
    
//    @IBAction func onGoToCart() {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
//        //        vc.product = product
//        present(vc,animated: true, completion: nil)
//    }
    func loadItem(){
        makeApiCall(api: "/product",
                    onSuccess: { response in
            
            let array = response as! [[String: Any]]
                        for obj in array {
                            let item = Product()
                            item.Pid = obj["Pid"] as? Int
                            item.Pname = obj["Pname"] as? String
                            item.Prate = obj["Prate"] as? Double
                            item.PQuantity = obj["PQuantity"] as? String
                            item.Pthumbnail = obj["Pthumbnail"] as? String
                            self.items.append(item)
                        }
                        
                        self.tableView.reloadData()
        }, method: .get)
    }

    
}

extension ItemListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        let item = items[indexPath.row]
        
        cell.labelName.text = item.Pname
        cell.labelPrice.text = "\(item.Prate!)"
        cell.labelQuantity.text = item.PQuantity
        
       let imageUrl = URL(string: url + "/\(item.Pthumbnail!)")
        
         cell.imageview.kf.setImage(with: imageUrl)
        cell.imageview.layer.cornerRadius = 10

        cell.buttonAdd.layer.shadowColor = Color.lightGray.cgColor
        cell.buttonAdd.layer.shadowOpacity = 0.8
        cell.buttonAdd.layer.shadowRadius = 10
      
        cell.onAddItem = { () in
            let body: [String: Any] = [
                "cust_id": AppPreferences.customerId,
                "Pid": item.Pid,
                "Prate": item.Prate
                ] as [String : Any]
            self.makeApiCall(api: "/cart", onSuccess: { response in
                self.view.makeToast("Added item to cart")
            }, parameters: body)
        }
        return cell
    }
}

extension ItemListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
