//
//  SearchViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 24/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class SearchViewController: BaseViewController {

    var products: [Product] = []

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var searchcollectionView: UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
  loadAllProducts()
    }
    
//    func loadAllProducts(){
//        makeApiCall(api: "/product/search",
//                    onSuccess: { response in
//
//                        let array = response as! [[String: Any]]
//                        for obj in array {
//                            let item = Product()
//                            item.Pid = obj["Pid"] as? Int
//                            item.Pname = obj["Pname"] as? String
//                            item.Prate = obj["Prate"] as? Float
//                            item.PQuantity = obj["PQuantity"] as? String
//                            item.Pthumbnail = obj["Pthumbnail"] as? String
//                            self.products.append(item)
//                        }
//
//                        self.searchcollectionView.reloadData()
//        }, method: .get)
//    }
    
    func loadAllProducts() {
        makeApiCall(api: "/product", onSuccess: { result in
            let array = result as! [[String: Any]]
            for item in array{
                let product = Product()
                product.Pname = item["Pname"] as? String
                product.Prate = item["Prate"] as? Double
                product.PQuantity = item["PQuantity"] as? String
                product.Pthumbnail = item["Pthumbnail"] as? String
                print(product.Pthumbnail)
                self.products.append(product)
            }
            self.searchcollectionView.reloadData()
        },method: .get)

    }

    @IBAction func onSearch() {
        if searchBar.text!.count == 0 {
            loadAllProducts()
        } else {
            view.makeToast("list is here")
            print(searchBar.text)
            
            makeApiCall(api: "/product/search/" + searchBar.text! ,onSuccess: { result in
                                //let result = result.value as! [String: Any]
                                self.products.removeAll()
                                print(result)
                                let array = result as! [[String: Any]]
                                for item in array {
                                    let product = Product()
                                    product.Pname = item["Pname"] as? String
                                    product.Prate = item["Prate"] as? Double
                                    product.PQuantity = item["PQunatity"] as? String
                                    product.Pthumbnail = item["Pthumbnail"] as? String
                                    self.products.append(product)
                                }

                                self.searchcollectionView.reloadData()
                                //                    self.searchBar.resignFirstResponder()
                            },method: .get)
                
                            view.makeToast("list is here")
                
                        }
                    }
            
        }

//    func searchProducts() {
//        if searchBar.text!.count == 0 {
//            loadAllProducts()
//        } else {
////            Alamofire
////                .request(url + "/product/search/" + searchBar.text!)
////                .responseJSON(completionHandler: {response in
////                    let result = response.result.value as! [String: Any]
////                    self.products.removeAll()
////
////                    let array = result["data"] as! [[String: Any]]
////                    for item in array {
////                        let product = Product()
////                        product.Pname = item["Pname"] as? String
////                        product.Prate = item["Prate"] as? Double
////                        product.PQuantity = item["PQunatity"] as? String
////                        product.Pthumbnail = item["Pthumbnail"] as? String
////                        self.products.append(product)
////                    }
////
////                    self.searchcollectionView.reloadData()
////                    //                    self.searchBar.resignFirstResponder()
////                })
//
//
////            makeApiCall(api: "/product/search/" + searchBar.text! ,onSuccess: { response in
////                let result = response.value as! [String: Any]
////                self.products.removeAll()
////
////                let array = result["data"] as! [[String: Any]]
////                for item in array {
////                    let product = Product()
////                    product.Pname = item["Pname"] as? String
////                    product.Prate = item["Prate"] as? Float
////                    product.PQuantity = item["PQunatity"] as? String
////                    product.Pthumbnail = item["Pthumbnail"] as? String
////                    self.products.append(product)
////                }
////
////                self.searchcollectionView.reloadData()
////                //                    self.searchBar.resignFirstResponder()
////            },method: .get)
//
//            view.makeToast("list is here")
//
//        }
//    }



extension SearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //searchProducts()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        loadAllProducts()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        loadAllProducts()
    }
}


extension SearchViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = searchcollectionView.dequeueReusableCell(withReuseIdentifier: "productsearchCell", for: indexPath) as! SearchCollectionViewCell
        let product = products[indexPath.row]
        cell.labelTitle.text = product.Pname

        let imageUrl = URL(string: url + "/\(product.Pthumbnail!)")
        cell.imageViewProduct.kf.setImage(with: imageUrl)
        cell.imageViewProduct.layer.cornerRadius = 10

        return cell
    }


}

extension SearchViewController: UICollectionViewDelegate {

}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (view.frame.width / 2) - 10
        return CGSize(width: width, height: 200)
    }

}
