//
//  BaseViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 19/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire

class BaseViewController: UIViewController {

    let url = "http://192.168.43.81:9000"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func showWarning(message: String){
        view.makeToast(message)
    }
    
    func makeApiCall(api: String, onSuccess:@escaping ((_ data: Any?) -> Void), onError: ((_ data:Any?) -> Void)? = nil ,method: HTTPMethod = .post, parameters: Parameters? = nil){
        Alamofire
            .request(url + "\(api)", method: method, parameters: parameters)
            .responseJSON(completionHandler: { response in
                let result = response.result.value as! [String: Any]
                if result["status"] as! String == "success" {
                    onSuccess(result["data"])
                }else {
                    if let onError = onError{
                        onError(result["data"])
                    }else{
                        self.showWarning(message: "api error")
                    }
                }
                
            })
    }
    
    func launchVC(withIdentifier id: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: id)
    }

}
