//
//  RegisterViewController.swift
//  Dairy_app
//
//  Created by sunbeam on 19/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {

    @IBOutlet weak var editFullname: UITextField!
    @IBOutlet weak var editAddress: UITextField!
    @IBOutlet weak var editPincode: UITextField!
    @IBOutlet weak var editPhone: UITextField!
    @IBOutlet weak var editEmailId: UITextField!
    @IBOutlet weak var editUsername: UITextField!
    @IBOutlet weak var editPassword: UITextField!
    @IBOutlet weak var editGender: UITextField!
    @IBOutlet weak var labelGender: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Registration"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save , target: self, action: #selector(onRegister))
    }

    @IBAction func onRegister() {
        let body = [
            "Cname": editFullname.text!,
            "Carea": editAddress.text!,
            "Pin": editPincode.text!,
            "Phone": editPhone.text!,
            "Cemail": editEmailId.text!,
            "Cusername": editUsername.text!,
            "Cpassword": editPassword.text!,
            //"Cgender": editGender.text!
            "Cgender": labelGender.selectedSegmentIndex == 0 ? "Male":"Female"
            
//                if labelGender.selectedSegmentIndex == 0{
//                    editGender.text = "Male"
//                }else{
//                    editGender.text = "Female"
//                }
        ]
        
        makeApiCall(api: "/customer/signup", onSuccess: {(result) in
            self.view.makeToast("Successfully registered")
            self.navigationController?.popViewController(animated: true)
        },parameters: body)

    }
    
    
    
}
