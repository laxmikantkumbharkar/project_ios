//
//  AppPreferences.swift
//  Dairy_app
//
//  Created by sunbeam on 20/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import Foundation

class AppPreferences{
    
    static let key_login_status = "login_status"
    static let key_customer_id = "customer_id"
    static let key_customer_email = "customer_email"
    static let key_customer_name = "username"
    
    class var isUserLoggedIn : Bool{
        if let status  = UserDefaults.standard.value(forKey: key_login_status) as? Bool {
            return status
        }else {
            return false
        }
    }
    
    class var customerId : Int{
        if let customerId = UserDefaults.standard.value(forKey: key_customer_id) as? Int{
            return customerId
        }else{
            return -1
        }
    }
    
    class func loginCustomer(Cname: String, Cemail: String, Id: Int){
        let userDefault = UserDefaults.standard
        userDefault.setValue(Cname, forKey: key_customer_name)
        userDefault.setValue(Cemail, forKey: key_customer_email)
        userDefault.setValue(Id, forKey: key_customer_id)
        userDefault.setValue(true, forKey: key_login_status)
        userDefault.synchronize()
    }
    
    class func logout(){
        let userDefault = UserDefaults.standard
        userDefault.setValue("", forKey: key_customer_name)
        userDefault.setValue("", forKey: key_customer_email)
        userDefault.setValue(-1 , forKey: key_customer_id)
        userDefault.setValue(false, forKey: key_login_status)
        userDefault.synchronize()
    }
    
    
}
