//
//  Product.swift
//  Dairy_app
//
//  Created by sunbeam on 19/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import Foundation

class Product{
    var Pid: Int!
    var Pname: String!
    var Prate: Double!
    var Pcategory: String!
    var PQuantity: String!
    var Pthumbnail: String!
    var Punit: Int!
    
//    init(Pid: Int, Pname: String,Prate: Float,Pcategory: String,PQuantity: String,Pthumbnail: String){
//        self.Pid = Pid
//        self.Pname = Pname
//        self.Prate = Prate
//        self.Pcategory = Pcategory
//        self.PQuantity = PQuantity
//        self.Pthumbnail = Pthumbnail
//    }
}
